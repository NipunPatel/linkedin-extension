import { AfterViewInit,Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'linkedinAng';
  public protocol = "https://";
  public primaryapi = "/gcu/primary";
  public loginUrl: any;
  @ViewChild('loginForm') 'form': ElementRef;
  @ViewChild('loading') 'loading': ElementRef;
  @ViewChild('loginError') 'loginError': ElementRef;
  @ViewChild('resultContainer') 'results': ElementRef;
  @ViewChild('username') 'username': ElementRef;
  @ViewChild('domainName') 'domain': ElementRef;
  @ViewChild('loginto') 'loginto': ElementRef;
  @ViewChild('icon') 'icon': ElementRef;
  @ViewChild('bodytag') 'body': ElementRef;
  @ViewChild('error') 'error': ElementRef;
  public gifCheck = false;
  public welCheck = false;
  public errorCheck = false;
  public logintoCheck = false;
  public usernameData: any = undefined

  async searchfordomain(userdomain: any){
    var part = userdomain.slice(0,8)
    if(part === "https://") {
        userdomain = userdomain.slice(8)
    }
    if(userdomain!=null)
    {
        this.form.nativeElement.style.display = "none"
    }
    this.form.nativeElement.style.display = "none"
   
    try {
        var buildurl = this.protocol + userdomain + this.primaryapi
        this.loginUrl = this.protocol + userdomain
        const getuser = await fetch(buildurl).then(response => response.json())
        .then(data => {
            var new_data = atob(data.data)
            var access_data = JSON.parse(new_data)
            chrome.storage.sync.set({primary: access_data});
            var sub_modules = access_data.subscribedModules
            this.form.nativeElement.style.display = "none"
            this.loading.nativeElement.style.display = "none"
            if (sub_modules.includes("happierHire")) {
                var firstname = access_data.userData.basicProfile.firstName
                var lastname = access_data.userData.basicProfile.lastName
                var fullname = firstname + ' ' + lastname
                this.welCheck = true 
                setTimeout(() => {
                  this.username.nativeElement.textContent = fullname
                  this.usernameData = fullname
                  chrome.storage.sync.set({userdomain: userdomain});
                  this.results.nativeElement.style = "padding-top:20px;"
                  this.results.nativeElement.style.display = "block"
                }, 0);  
                // chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                //     chrome.tabs.reload();
                //     window.close();
                // });
                
            }else {
                this.errorCheck = true
                setTimeout(() => {
                  this.loading.nativeElement.style.display = "none";
                  this.loginError.nativeElement.style = "position: absolute; top: 45%; left: 25%;"
                  this.loginError.nativeElement.textContent = "happierHire not subscribed";  
                }, 10);
            }
        });
    }
     catch {
        this.errorCheck = true
        this.logintoCheck = true
        setTimeout(() => {
          this.icon.nativeElement.style.marginTop = "40px"
          this.loading.nativeElement.style.display = "none";
          this.form.nativeElement.style.display = "none";
          this.loginError.nativeElement.textContent = "We observed you have not logged into";
          this.loginto.nativeElement.textContent = " Please login and then open this extension"
          // loginLink.textContent = loginUrl
          var mydiv = document.getElementsByClassName("error");
          var aTag = document.createElement('a');
          aTag.setAttribute('href',this.loginUrl);
          aTag.textContent= this.loginUrl;
          aTag.style.height = "40px"
          aTag.style.fontFamily = "Noto Sans"
          aTag.style.fontSize = "14px"
          aTag.target = "_blank"
          mydiv[0].appendChild(aTag);
        }, 10); 
     }
};

async handleSubmit(e: any) {
   e.preventDefault();
    try {
        this.gifCheck = true
        this.searchfordomain(this.domain.nativeElement.value);     
      }catch (error) {
        console.log(error)
    }
};
}
