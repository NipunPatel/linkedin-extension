// import type {Configuration} from 'webpack'
// module.exports = {
//     entry: {linkedin: {import: './src/linkedin.ts', runtime:false}}
// } as Configuration

import type {Configuration} from 'webpack'
module.exports = {
entry :{
    linkedin: {import : './src/linkedin.ts'},
    background: {import: './src/background.ts',runtime:false}    
}
} as Configuration